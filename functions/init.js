const admin = require('firebase-admin');
const serviceAccount = require("./serviceAccountKey.json");

const data = require(process.env.FILE_NAME);
const collectionKey = "companies";

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: `https://${process.env.FIREBASE_DB_ENDPOINT}.firebaseio.com`
});

const firestore = admin.firestore();
const settings = {
    timestampsInSnapshots: true
};

firestore.settings(settings);

if (data && (typeof data === "object")) {
    Object.keys(data).forEach(docKey => {
        firestore.collection(collectionKey).doc(docKey).set(data[docKey]).then((res) => {
            console.log("Document " + docKey + " successfully written!");
        }).catch((error) => {
            console.error("Error writing document: ", error);
        });
    });
}