'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
const cors = require('cors')({ origin: true });

admin.initializeApp();

exports.filter = functions.https.onRequest(async (req, res) => {
    cors(req, res, async () => {
        if (!Object.keys(req.query).length) {
            res.status(400).json({ error: true, message: 'Not a valid query.' });
        }

        const collection = admin.firestore().collection('companies').withConverter()

        const resp = await Object.entries(req.query).reduce((acc, [key, value]) => {
            return acc.where(key.toUpperCase(), "==", value)
        }, collection).get();

        const getItemList = (data) => {
            if (data._size === 0) {
                return Promise.resolve([])
            }

            return data._docs().map(doc => ({
                id: doc.id,
                ...doc.data(),
            })
            )
        }

        res.json({
            data: await getItemList(resp)
        })
    })
});