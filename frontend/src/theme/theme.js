import { createMuiTheme } from "@material-ui/core/styles";
import {
  grey,
} from "@material-ui/core/colors";

const rawTheme = createMuiTheme({
  palette: {
    primary: {
      main: "#002dac",
      dark: '#002078',
    },
    secondary: {
      main: "#00154f",
    },
    text: {
      primary: '#00154f',
      contrastText: '#00154f'
    },
    action: {
      hover: '#8C7974',
      focus: '#8C7974',
      selected: '#8C7974',
      active: '#8C7974'
    }
  },
  typography: {
    fontFamily: "'Roboto', sans-serif;",
    body1: {
      fontFamily: "'Roboto', sans-serif;"
    },
    body2: {
      fontFamily: "'Roboto', sans-serif;"
    },
    fontSize: 14,
    fontWeightLight: 300, // Work Sans
    fontWeightRegular: 500, // Work Sans
    fontWeightMedium: 700,
    fontFamilyPrimary: "'Roboto', sans-serif;",
    fontFamilySecondary: "'Roboto', sans-serif;",
  },
});

const fontHeader = {
  color: rawTheme.palette.text.primary,
  fontWeight: 400,
  fontFamily: "'Roboto', sans-serif;",
  textTransform: "uppercase",
};

const theme = {
  ...rawTheme,
  palette: {
    ...rawTheme.palette,
    background: {
      ...rawTheme.palette.background,
      default: rawTheme.palette.common.white,
      placeholder: grey[200],
    },
  },
  typography: {
    ...rawTheme.typography,
    ...fontHeader,
    h1: {
      ...rawTheme.typography.h1,
      ...fontHeader,
      letterSpacing: 0,
      fontSize: 60,
      [rawTheme.breakpoints.down("sm")]: {
        fontSize: "2.5rem!important",
      },
    },
    h2: {
      ...rawTheme.typography.h2,
      ...fontHeader,
      fontSize: 48,
      [rawTheme.breakpoints.down("sm")]: {
        fontSize: "2.1rem!important",
      },
    },
    h3: {
      ...rawTheme.typography.h3,
      ...fontHeader,
      fontSize: 42,
      [rawTheme.breakpoints.down("sm")]: {
        fontSize: "2rem!important",
      },
    },
    h4: {
      ...rawTheme.typography.h4,
      ...fontHeader,
      fontSize: 36,
      [rawTheme.breakpoints.down("sm")]: {
        fontSize: "1.9rem!important",
      },
    },
    h5: {
      ...rawTheme.typography.h5,
      ...fontHeader,
      fontSize: 20,
      fontWeight: 500,
      [rawTheme.breakpoints.down("sm")]: {
        fontSize: "1.5rem!important",
      },
    },
    h6: {
      ...rawTheme.typography.h6,
      ...fontHeader,
      fontSize: 18,
      [rawTheme.breakpoints.down("sm")]: {
        fontSize: "1.1rem!important",
      },
    },
    subtitle1: {
      ...rawTheme.typography.subtitle1,
      fontSize: 18,
      [rawTheme.breakpoints.down("sm")]: {
        fontSize: "1.1rem!important",
      },
    },
    body1: {
      ...rawTheme.typography.body2,
      fontWeight: 500,
      fontSize: 16,
      [rawTheme.breakpoints.down("sm")]: {
        fontSize: "1rem!important",
      },
    },
    body2: {
      ...rawTheme.typography.body1,
      fontSize: 14,
      [rawTheme.breakpoints.down("sm")]: {
        fontSize: ".9rem!important",
      },
    },
  },
};

export default theme;
