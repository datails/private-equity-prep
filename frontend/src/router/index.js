import React from 'react';
import { useRoutes, useInterceptor } from 'hookrouter'
import AppHome from '../pages/home';
import AppCompany from '../pages/country';
import AppCountries from '../pages/countries';
import AppCoaching from '../pages/coaching';
import Page404 from '../pages/404'

const routes = {
  '/': () => <AppHome />,
  '/country': () => <AppCountries />,
  '/country/:countryName': ({ countryName }) => <AppCompany countryName={decodeURI(countryName)} />,
  '/coaching': () => <AppCoaching />
};

const App = () => {
  useInterceptor((currentPath, nextPath) => {
    window.scrollTo(0, 0);
    return nextPath
  })

  const routeResult = useRoutes(routes);
  return routeResult || <Page404 />;
}

export default App;
