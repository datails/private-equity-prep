import React from "react";
import { navigate } from 'hookrouter';
import { VectorMap } from '@react-jvectormap/core';
import { europeMill } from '@react-jvectormap/europe';
import { getName } from 'country-list';

const colors = (map) => {
    const palette = ['#002dac', '#4A78F7', '#002078', '#375AB9', '#0042F7', '#ffbf00', '#FFD24D', '#806926', '#CC9900'];

    return Object.keys(map).reduce((acc, curr) => ({
        ...acc,
        [curr]: palette[Math.floor(Math.random() * palette.length)]
    }), {});
};

function Map({ backgroundColor = 'transparent' }) {
    const handleClick = (_event, countryCode) => {
        navigate(`/country/${getName(countryCode)}`)
    };

    return (
        <VectorMap
            zoomOnScroll={false}
            zoomButtons={false}
            map={europeMill}
            backgroundColor={backgroundColor}
            onRegionClick={handleClick}
            regionsSelectableOne
            regionStyle={colors(europeMill.content.paths)}
            series={{
                regions: [{
                    values: colors(europeMill.content.paths),
                    attribute: 'fill',
                }]
            }}
        />
    )
}

export default Map