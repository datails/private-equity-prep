import React from 'react';
import { Grid, Link, Typography } from '@material-ui/core';
import { LinkedIn } from '@material-ui/icons'
import classNames from "classnames";
import useStyles from './footer.styles';
import withRoot from '../../theme/withRoot';
import { COURSE_URI } from '../../constants'

function Footer() {
  const classes = useStyles();

  return (
    <>
      <Grid container spacing={10} className={classes.container} justify="space-around">
        <Grid item xs={12} sm={12} md={4} className={classes.content}>
          <Typography variant="h2" gutterBottom className={classes.title}>
            About
          </Typography>
          <Typography variant="body1" gutterBottom className={classes.text}>
            I'm a professional private equity banker.
          </Typography>
        </Grid>
        <Grid item xs={12} sm={12} md={4} className={classes.content}>
          <Typography variant="h2" gutterBottom className={classes.title}>
            Nav
          </Typography>
          <Typography variant="body1" className={classes.anchor}>
            <Link href={COURSE_URI} target="_blank" color="inherit">
              Take the course
            </Link>
            <Link href="/countries" color="inherit">
              Search the map
            </Link>
          </Typography>
        </Grid>
        <Grid item xs={12} sm={12} md={4} className={classes.content}>
          <Typography variant="h2" gutterBottom className={classes.title}>
            Contact
          </Typography>
          <Typography variant="body1" className={classes.anchor}>
            <Link href="mailto:info@private-equity-prep.com" target="_blank" color="inherit">
              info@private-equity-prep.com
            </Link>
            <Grid container spacing={10} justify="flex-start">
              <Grid item xs={1}>
                <Link href="https://www.linkedin.com/in/david-van-de-fliert-532b7989/" target="_blank" color="inherit">
                  <LinkedIn />
                </Link>
              </Grid>
            </Grid>
          </Typography>
        </Grid>
      </Grid>
      <Grid
        container
        className={classNames([classes.container, classes.backgroundMain])}
        justify="space-around"
      >
        <Grid item xs={12} className={classes.content}>
          <Typography variant="body1" className={classes.anchor}>
            <Link
              href="//datails.nl"
              target="_blank"
              color="inherit"
            >
              Powered By DATAILS.
            </Link>
          </Typography>
        </Grid>
      </Grid>
    </>
  );
}

export default withRoot(Footer);