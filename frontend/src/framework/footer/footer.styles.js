import { createStyles, makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) =>
  createStyles({
    anchor: {
      color: '#FFF',
      display: 'flex',
      flexDirection: 'column',
      fontSize: '1rem',
      lineHeight: 2,
    },
    container: {
      background: '#4a5983',
      margin: '0',
      width: '100%',
      padding: '40px',
    },
    content: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'flex-start',
      padding: '40px',
    },
    margin: {
      margin: theme.spacing(1),
    },
    title: {
      color: '#FFF',
      fontSize: '1.5rem',
      fontWeight: 500,
      marginBottom: '1.5rem',
    },
    text: {
      color: '#FFF',
      fontSize: '1rem',
      lineHeight: 1.2,
      textAlign: 'justify',
    },
    backgroundMain: {
      padding: "0",
      textAlign: "center",
      background: "#00154f",
    },
  }),
);