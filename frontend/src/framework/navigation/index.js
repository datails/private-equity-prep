import React, { useState, useEffect, useContext } from "react";
import {
  AppBar,
  Toolbar,
  IconButton,
  List,
  ListItem,
  ListItemText,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import classNames from "classnames";
import { navigate } from "hookrouter";
import { Context } from "../../store/store";
import withRoot from "../../theme/withRoot";
import useStyles from "./navigation.styles";
import { COURSE_URI } from "../../constants";

function AppNavigation() {
  const classes = useStyles();

  const [, dispatch] = useContext(Context);
  const [atTopOfPage, setAtTopOfPage] = useState(true);
  const [sleep, setSleep] = useState(false);

  const handleScroll = () => {
    setAtTopOfPage(window.pageYOffset < 275 ? true : false);
    setSleep(window.pageYOffset < 350 ? true : false);
  };

  const toggleDrawer = () => {
    dispatch({
      type: "TOGGLE_DRAWER",
    });
  };

  useEffect(() => {
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  useEffect(() => window.addEventListener("scroll", handleScroll), []);

  return (
    <div className={classes.root}>
      <AppBar
        color="default"
        position="relative"
        className={
          atTopOfPage
            ? classes.navigationBarInitial
            : sleep
              ? classNames(classes.navigationBar, classes.navigationBarHide)
              : classes.navigationBar
        }
      >
        <Toolbar className={classes.navigationBarInner}>
          <a
            href="/"
            className={
              atTopOfPage
                ? classes.heartBeatIcon
                : classNames(classes.heartBeatIcon, classes.heartBeatIconAwake)
            }
          >
            <span
              className={
                atTopOfPage
                  ? classes.logo
                  : classNames(classes.logo, classes.logoRevert)
              }
            ></span>
          </a>
          <List
            component="nav"
            className={
              atTopOfPage
                ? classNames(classes.hiddenMdDown, classes.navList)
                : classes.displayNone
            }
          >
            <ListItem button onClick={() => window.open(COURSE_URI)}>
              <ListItemText primary={"Start course"} />
            </ListItem>
            <ListItem button onClick={() => navigate("/country")}>
              <ListItemText primary={"Search companies"} />
            </ListItem>
            <ListItem button onClick={() => navigate("/coaching")}>
              <ListItemText primary={"1 on 1 coaching"} />
            </ListItem>
          </List>
          <div>
            <IconButton
              edge="start"
              className={
                atTopOfPage
                  ? classNames(classes.menuButton, classes.hiddenMdUp)
                  : classNames(classes.menuButton, classes.colorDark)
              }
              color="inherit"
              aria-label="menu"
              onClick={toggleDrawer}
            >
              <MenuIcon
                className={atTopOfPage ? classes.colorWhite : classes.colorDark}
              />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      <div className={atTopOfPage ? classes.offset0 : classes.offset}></div>
    </div>
  );
}

export default withRoot(AppNavigation);
