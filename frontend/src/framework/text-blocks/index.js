import React from 'react';
import { Divider } from '@material-ui/core';
import { ImageBlock } from '../../components';
import withRoot from '../../theme/withRoot';
import { even } from '../../utils'

function TextBlocks({ content, children, backgroundColor }) {
    return content.map((props, index) => {
        const isEven = even(index);
        return (
            <>
                <ImageBlock
                    reverse={isEven}
                    backgroundColor={backgroundColor || !isEven && "#ebebeb"}
                    {...props}
                >
                    {children}
                </ImageBlock>
                <Divider />
            </>
        )
    })
}

export default withRoot(TextBlocks)