import React from "react";
import { Grid, Paper, Typography, Avatar } from "@material-ui/core";
import classNames from "classnames";
import SchoolIcon from "@material-ui/icons/School";

import AppButton from "../../components/app-button";
import withRoot from '../../theme/withRoot';
import { COURSE_URI } from '../../constants';
import useStyles from './banner.styles';

function ComponentTopBanner({ backgroundColor }) {
  const classes = useStyles();

  return (
    <Grid
      container
      justify="center"
      alignItems="center"
      className={classes.container}
      style={{ backgroundColor: backgroundColor }}
    >
      <Grid
        item
        xs={12}
        sm={12}
        md={10}
        xl={8}
        justify="center"
        alignItems="center"
      >
        <Paper className={classes.paperContainer}>
          <Grid
            item
            xs={12}
            sm={3}
            md={3}
            justify="center"
            alignItems="center"
            className={classes.flex}
          >
            <Avatar
              className={classes.avatar}
              alt="Take the course!"
            >
              <SchoolIcon className={classes.icon} />
            </Avatar>
          </Grid>
          <Grid
            item
            xs={12}
            sm={9}
            md={6}
            justify="center"
            alignItems="start"
            className={classes.flex}
            direction="column"
          >
            <Typography
              component="h2"
              variant="h6"
              className={classNames(classes.contentText, classes.fontWeight700, classes.colorDark)}
            >
              Take the course!
            </Typography>
            <Typography
              component="h3"
              variant="subtitle1"
              className={classNames(classes.contentText, classes.color)}
            >
              Prepare your interviews in the private equity markets. Take the course today and get the job you deserve.
            </Typography>
          </Grid>
          <Grid
            item
            xs={12}
            sm={12}
            md={3}
            justify="center"
            alignItems="center"
            className={classes.flex}
          >
            <AppButton
              href={COURSE_URI}
              target="_blank"
              styles={{
                backgroundColor: '#FFD24D',
                color: '#00154f',
              }}
            >
              Start
            </AppButton>
          </Grid>
        </Paper>
      </Grid>
    </Grid>
  );
}

export default withRoot(ComponentTopBanner)