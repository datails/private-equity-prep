import { COUNTRY_ENDPOINT } from '../constants'

export const getCompaniesPerCountry = async (countryName) => {
    return ((await fetch(`${COUNTRY_ENDPOINT}?country=${countryName}`)).json()).catch(console.error);
}