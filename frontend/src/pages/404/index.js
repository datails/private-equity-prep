import React from "react";
import { Divider } from "@material-ui/core";
import { Breadcrumb, Banner, Header, TextBlock } from "../../framework";
import content from './404.content';
import withRoot from "../../theme/withRoot";

function AppNotFound() {
  return (
    <React.Fragment>
      <Divider />
      <Header content={content.header} />
      <Divider />
      <Breadcrumb backgroundColor="#FFF1E3" current="404 niet gevonden" />
      <Divider />
      <TextBlock content={content.content} />
      <Banner />
    </React.Fragment>
  );
}

export default withRoot(AppNotFound)