export default {
    header: {
        title: 'Page not found!',
        subTitle: '404 not found',
        background: 'europe_union.jpeg',
    },
    content: [
        {
            title: 'Page not found!',
            text: 'Sorry couldn\'t find this page.'
        }
    ]
}