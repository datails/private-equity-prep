import React, { useContext, useEffect } from "react";
import CountryContext from "./country.context";
import { getCompaniesPerCountry } from "../../utils/api";
import { Loader } from "../../components";
import withRoot from "../../theme/withRoot";

function CountrySwitch({ countryName }) {
  const { setCompanies } = useContext(CountryContext);

  const loadProperty = async () => {
    const { data } = await getCompaniesPerCountry(countryName);
    setCompanies(data);
  };

  useEffect(() => {
    loadProperty();
  }, []);

  return <Loader />;
}

export default withRoot(CountrySwitch);
