import React from "react";

const CountryContext = React.createContext({
  companies: [],
  setCompanies: () => {},
});

export default CountryContext;
