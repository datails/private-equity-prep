import * as React from 'react';
import { DataGrid, GridToolbar } from '@material-ui/data-grid';
import withRoot from '../../../../theme/withRoot';
import useStyles from './table.styles';

const columns = require('./columns.json');

function Table({ rows }) {
  const classes = useStyles();

  return (
      <DataGrid
        components={{
          Toolbar: GridToolbar,
        }}
        rows={rows}
        columns={columns}
        pageSize={15}
        rowsPerPageOptions={[5]}
        checkboxSelection
        disableSelectionOnClick
        className={classes.container}
        autoHeight={true}
        autoPageSize={true}
        // filterModel={{
        //   items: [{ columnField: 'Total assets under management (Curr MN)', operatorValue: 'contains', value: 100 }],
        // }}
      />
  );
}

export default withRoot(Table)