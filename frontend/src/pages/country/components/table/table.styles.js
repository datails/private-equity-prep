import { makeStyles, createStyles } from "@material-ui/core";

export default makeStyles((theme) =>
  createStyles({
    container: {
      color: theme.palette.secondary.dark
    },
  })
);
