import React, { useState } from "react";
import { Grid, Divider } from "@material-ui/core";
import { Breadcrumb } from "../../framework";
import {
  Table,
} from "./components";
import withRoot from "../../theme/withRoot";

import CountryContext from "./country.context";
import CountrySwitch from "./country.switch";
import useStyles from "./country.styles";

const Country = ({ countryName }) => {
  const classes = useStyles();
  const [companies, setCompanies] = useState();
  const value = { companies, setCompanies };

  if (!companies)
    return (
      <CountryContext.Provider value={value}>
        <CountrySwitch countryName={countryName} />
      </CountryContext.Provider>
    );

  return (
    <main>
      <Breadcrumb backgroundColor="#4092B8" current={countryName} />
      <Divider />
      <Grid
        container
        className={classes.container}
        justify="space-evenly"
        spacing={10}
      >
        <Grid item xs={12} md={12} justify="center" align="center">
          <Table rows={companies} />
        </Grid>
      </Grid>
    </main>
  );
};

export default withRoot(Country);
