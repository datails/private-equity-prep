import React from 'react'

export default {
    content: [
        {
            title: 'About PEP',
            text: `Hi, I'm David van de Fliert a private equity banker. I'm here to help you getting the job you deserve.`,
            html: <img src={require('../../assets/david_van_de_fliert.jpeg')} />
        }
    ]
}