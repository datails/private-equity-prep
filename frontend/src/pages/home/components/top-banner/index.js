import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import { Card } from '../../../../components';
import withRoot from '../../../../theme/withRoot'
import useStyles from './banner.styles';

function TopBanner() {
  const classes = useStyles();

  return (
    <Grid container spacing={10} className={classes.container} justify="space-evenly">
      <Grid item sm={12} justify="center" align="center">
        <Typography
          variant="h2"
          component="h2"
          gutterBottom
          className={classes.title}
        >
          Prepare your private equity interviews
        </Typography>
        <Typography variant="subtitle1" component="h3">
          The private equity market is a traditional and conservative market. Get hired by preparing your interview the right way.
        </Typography>
      </Grid>
      <Grid item xs={12} sm={12} md={6} lg={4} className={classes.content}>
        <Card
          buttonText="Search"
          desc="Find private equity companies in Europe."
          image={require('../../../../assets/europe_union.jpeg')}
          title="Search companies"
          href={'/country'}
          buttonStyle={{
            backgroundColor: '#00154f',
          }}
        />
      </Grid>
      <Grid item xs={12} sm={12} md={6} lg={4} className={classes.content}>
        <Card
          buttonText="Start"
          desc="Start the video course and prepare yourself for your interviews."
          image={require('../../../../assets/course.jpeg')}
          title="Take course"
          href={'https://udemy.com'}
          buttonStyle={{
            backgroundColor: '#00154f',
          }}
        />
      </Grid>
      <Grid item xs={12} sm={12} md={6} lg={4} className={classes.content}>
        <Card
          buttonText="Contact"
          desc="Get 1-on-1 coaching from an expert of the field."
          image={require('../../../../assets/coaching.jpeg')}
          title="Coaching"
          href={'/coaching'}
          buttonStyle={{
            backgroundColor: '#00154f',
          }}
        />
      </Grid>
    </Grid>
  );
}

export default withRoot(TopBanner)