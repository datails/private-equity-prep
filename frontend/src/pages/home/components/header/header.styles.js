import { createStyles, makeStyles } from "@material-ui/core";

export default makeStyles((theme) =>
  createStyles({
    colorWhite: {
      color: "#fff",
    },
    container: {
      minHeight: "100vh",
      margin: "-65px 0 0",
      width: "100%",
      display: 'flex',
      justifyContent: 'center',
      backgroundImage: `url(${require(`../../../../assets/finance_header.jpg`)})`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
      backgroundAttachment: "fixed",
      backgroundPosition: "center",
      [theme.breakpoints.down("sm")]: {
        minHeight: "50vh",
        backgroundAttachment: "inherit",
      },
    },
    content: {
      alignItems: "flex-start",
      display: "flex",
      textAlign: "left",
      flexDirection: "column",
      justifyContent: "center",
      maxWidth: "550px",
      textAlign: 'left',
      textShadow: '-1px 2px #020202',
      color: '#FFF1E3',
      [theme.breakpoints.down("sm")]: {
        marginLeft: "0px",
        maxWidth: "100%",
        textAlign: "left",
      },
    },
    map: {
      [theme.breakpoints.down("sm")]: {
        display: 'none',
      },
    },
    logo: {
      height: '280px',
      [theme.breakpoints.down("md")]: {
        height: "auto",
      },
    },
    title: {
      color: "#FFF",
      fontSize: "28px",
    },
    subTitle: {
      color: "#FFF",
      fontSize: ".8rem",
      fontSize: "18px",
    },
  })
);