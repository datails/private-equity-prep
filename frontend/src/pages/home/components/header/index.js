import React from "react";
import { Grid, Typography } from "@material-ui/core";
import classNames from "classnames";
import useStyles from './header.styles';
import withRoot from "../../../../theme/withRoot";
import { WorldMap } from '../../../../framework';
import { Button } from '../../../../components';
import { COURSE_URI } from '../../../../constants';


function Header() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Grid container spacing={10} className={classes.container}>
        <Grid item xs={12} sm={8} md={6} className={classes.content} justify="flex-start">
          <Typography

            component="h1"
            gutterBottom
            className={classes.title}
          >
            Land a job within a leading European Private Equity manager
          </Typography>
          <Typography
            variant="subtitle1"
            component="p"
            gutterBottom
            className={classes.subTitle}
          >
            Get your position in the private equity banking. Start the course.
          </Typography>
          <Button href={COURSE_URI}>
            Start
          </Button>
        </Grid>
        <Grid justify="center" item xs={0} sm={4} md={6} className={classNames(classes.content, classes.map)} display={{ xs: "none" }}>
          <WorldMap />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default withRoot(Header);