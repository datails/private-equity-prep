import React from 'react';
import { Helmet } from 'react-helmet'
import { Divider } from '@material-ui/core';
import { Breadcrumb, Header, TextBlock, Banner, WorldMap } from '../../framework'
import content from './countries.content';
import withRoot from '../../theme/withRoot';

function AppCountries() {
  return (
    <React.Fragment>
      <Helmet>
        <title>Find private equity companies in Europe</title>
        <meta name="description" content="Find the private equity company in Europe including all relevant resources." />
        <meta property="og:description" content="Find the private equity company in Europe including all relevant resources." />
        <meta property="og:title" content="Find private equity companies in Europe" />
        <meta property="og:url" content={"https://private-equity-preparation.com/countries"} />
      </Helmet>
      <Header content={content.header} />
      <Divider />
      <Breadcrumb />
      <Divider />
      <TextBlock content={content.content}>
        <div style={{ height: 600, width: 600 }}>
          <WorldMap />
        </div>,
      </TextBlock>
      <Banner />
      <Divider />
    </React.Fragment>
  );
}

export default withRoot(AppCountries)