export default {
    header: {
        title: 'Search private equity companies',
        subTitle: 'Get a full picture of companies in Europe',
        background: 'europe_union.jpeg'
    },
    content: [
        {
            title: 'Search',
            text: `Click on a country to search for private equity companies in the database.`,
        },
    ]
}