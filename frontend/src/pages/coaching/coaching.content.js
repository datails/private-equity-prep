import React from 'react'
import { TimeLine } from '../../framework'

export default {
    header: {
        title: '1 on 1 interview coaching',
        subTitle: 'Meet an expert to get a role in private equity',
        background: 'coaching.jpeg'
    },
    content: [
        {
            title: 'Meet David van de Fliert',
            text: `Hi i'm david, and this is the about me section.`,
            html: <img src={require('../../assets/david_van_de_fliert.jpeg')} />
        },
        {
            title: 'Career path',
            html: <TimeLine />
        }
    ]
}