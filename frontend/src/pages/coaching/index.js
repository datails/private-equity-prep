import React from 'react';
import { Helmet } from 'react-helmet'
import { Divider } from '@material-ui/core';
import { Breadcrumb, Header, TextBlock, Banner } from '../../framework'
import content from './coaching.content';
import withRoot from '../../theme/withRoot';

function AppAbout() {
  return (
    <React.Fragment>
      <Helmet>
        <title>1 on 1 coaching for private equity interview preparation</title>
        <meta name="description" content="Get one on one coaching by David van de Fliert to prepare for your private equity interviews." />
        <meta property="og:description" content="Get one on one coaching by David van de Fliert to prepare for your private equity interviews." />
        <meta property="og:title" content="1 on 1 coaching for private equity interview preparation" />
        <meta property="og:url" content={"https://private-equity-banking.com/coaching"} />
      </Helmet>
      <Header content={content.header} />
      <Divider />
      <Breadcrumb backgroundColor="#E8D0C3" />
      <Divider style={{ backgroundColor: 'rgb(128 120 113)' }} />
      <TextBlock content={content.content} />
      <Banner />
      <Divider />
    </React.Fragment>
  );
}

export default withRoot(AppAbout)